import { Test, TestingModule } from '@nestjs/testing';
import { VerificationResultService } from './verification-result.service';

describe('VerificationResultService', () => {
  let service: VerificationResultService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VerificationResultService],
    }).compile();

    service = module.get<VerificationResultService>(VerificationResultService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
