import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { VerificationResultService } from './verification-result.service';
import { CreateVerificationResultDto } from './dto/create-verification-result.dto';
import { UpdateVerificationResultDto } from './dto/update-verification-result.dto';

@Controller('verification-result')
export class VerificationResultController {
  constructor(
    private readonly verificationResultService: VerificationResultService,
  ) {}

  @Post()
  create(@Body() createVerificationResultDto: CreateVerificationResultDto) {
    return this.verificationResultService.create(createVerificationResultDto);
  }

  @Get()
  findAll() {
    return this.verificationResultService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.verificationResultService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateVerificationResultDto: UpdateVerificationResultDto,
  ) {
    return this.verificationResultService.update(
      +id,
      updateVerificationResultDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.verificationResultService.remove(+id);
  }
}
