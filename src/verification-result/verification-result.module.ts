import { Module } from '@nestjs/common';
import { VerificationResultService } from './verification-result.service';
import { VerificationResultController } from './verification-result.controller';

@Module({
  controllers: [VerificationResultController],
  providers: [VerificationResultService],
})
export class VerificationResultModule {}
