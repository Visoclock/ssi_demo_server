import { Test, TestingModule } from '@nestjs/testing';
import { VerificationResultController } from './verification-result.controller';
import { VerificationResultService } from './verification-result.service';

describe('VerificationResultController', () => {
  let controller: VerificationResultController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VerificationResultController],
      providers: [VerificationResultService],
    }).compile();

    controller = module.get<VerificationResultController>(
      VerificationResultController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
