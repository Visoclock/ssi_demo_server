import { Injectable } from '@nestjs/common';
import { CreateVerificationResultDto } from './dto/create-verification-result.dto';
import { UpdateVerificationResultDto } from './dto/update-verification-result.dto';

@Injectable()
export class VerificationResultService {
  create(createVerificationResultDto: CreateVerificationResultDto) {
    return 'This action adds a new verificationResult';
  }

  findAll() {
    return `This action returns all verificationResult`;
  }

  findOne(id: number) {
    return `This action returns a #${id} verificationResult`;
  }

  update(id: number, updateVerificationResultDto: UpdateVerificationResultDto) {
    return `This action updates a #${id} verificationResult`;
  }

  remove(id: number) {
    return `This action removes a #${id} verificationResult`;
  }
}
