import { PartialType } from '@nestjs/mapped-types';
import { CreateVerificationResultDto } from './create-verification-result.dto';

export class UpdateVerificationResultDto extends PartialType(
  CreateVerificationResultDto,
) {}
