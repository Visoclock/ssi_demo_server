import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SpeakerModule } from './speaker/speaker.module';
import { VerificationRequestModule } from './verification-request/verification-request.module';
import { VerificationResultModule } from './verification-result/verification-result.module';
import { WalletModule } from './wallet/wallet.module';
import { ProviderModule } from './provider/provider.module';
import { TenantModule } from './tenant/tenant.module';
import { CredentialsModule } from './credentials/credentials.module';
import { VerificationModule } from './verification/verification.module';
import { ScanResultModule } from './scan-result/scan-result.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    SpeakerModule,
    VerificationRequestModule,
    VerificationResultModule,
    WalletModule,
    ProviderModule,
    TenantModule,
    CredentialsModule,
    VerificationModule,
    ScanResultModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
