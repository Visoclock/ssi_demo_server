export interface IMessageEvent {
  data: string | Record<string, any>;
  id?: string;
  type?: string;
  retry?: number;
}
