export interface User {
  image_url: string;
  firstname: string;
  lastname: string;
  mail_sends: string;
  primary_email: string;
  pushToken: string;
  ref_code: string;
  use_vocus: boolean;
  uuid: string;
}
