import {
  CredentialsServiceClient,
  ProviderServiceClient,
  WalletServiceClient,
  Credentials,
  ProviderCredentials,
} from '@trinsic/service-clients';

const apiKey = 'k2OXZebTeHI7y6GwXeVmf5LtdaU-dThXHP3yr35jfM0';
const providerKey = 'C_6HOamZR2zGHTQO1wM4hMrQFsO6y1_H47fOlugZAok';

// Credentials API
export const credentialsClient = new CredentialsServiceClient(
  new Credentials(apiKey),
  { noRetryPolicy: true },
);

// Provider API
export const providerClient = new ProviderServiceClient(
  new ProviderCredentials(providerKey),
  { noRetryPolicy: true },
);

// Wallet API
export const walletClient = new WalletServiceClient(new Credentials(apiKey), {
  noRetryPolicy: true,
});
