import * as admin from 'firebase-admin';
import * as serviceAccount from '../service-account.json';
import firebase from 'firebase';
import * as dotEnv from 'dotenv';
import * as functions from 'firebase-functions';

dotEnv.config();

const params = {
  type: serviceAccount.type,
  projectId: serviceAccount.project_id,
  privateKeyId: serviceAccount.private_key_id,
  privateKey: serviceAccount.private_key,
  clientEmail: serviceAccount.client_email,
  clientId: serviceAccount.client_id,
  authUri: serviceAccount.auth_uri,
  tokenUri: serviceAccount.token_uri,
  authProviderX509CertUrl: serviceAccount.auth_provider_x509_cert_url,
  clientC509CertUrl: serviceAccount.client_x509_cert_url,
};

// const firebaseConfig = {
//   apiKey: process.env.fbAPIKey,
//   authDomain: process.env.fbAuthDomain,
//   databaseURL: process.env.fbDbURL,
//   projectId: process.env.fbProjectId,
//   storageBucket: process.env.fbStorageBucket,
//   messagingSenderId: process.env.fbMessagingSenderId,
//   appId: process.env.fbAppId,
//   measurementId: process.env.fbMeasurementId,
// };

export const firebaseAdmin = admin.initializeApp({
  credential: admin.credential.cert(params),
  storageBucket: 'veriself.appspot.com',
});

admin.firestore().settings({ ignoreUndefinedProperties: true });

export const adminTypes = admin;

// export const firebaseClient = firebase.initializeApp(firebaseConfig);

export const logger = functions.logger;

export const firebaseFunctions = functions;
