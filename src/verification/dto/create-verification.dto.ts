import { IsDefined } from 'class-validator';

export class CreateVerificationDto {
  @IsDefined()
  policyId: string;
}
