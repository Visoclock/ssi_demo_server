export class SubmitAutoVerificationDto {
  walletId: string;
  verificationId: string;
}
