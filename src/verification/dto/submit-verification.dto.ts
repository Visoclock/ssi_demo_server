export class SubmitVerificationDto {
  walletId: string;
  verificationId: string;
  connectionId?: string;
  policyName: string;
  credentialId: string;
  hidden: boolean;
}
