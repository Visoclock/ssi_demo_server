export class SendVerificationDto {
  connectionId: string;
  verificationId: string;
}
