import { Injectable, HttpStatus } from '@nestjs/common';
import { CreateVerificationDto } from './dto/create-verification.dto';
import { UpdateVerificationDto } from './dto/update-verification.dto';
import { credentialsClient, walletClient } from '../commons/trinsicConfig';
import { logger } from '../commons/firebaseConfig';
import {
  CreateVerificationFromPolicyResponse,
  ListConnectionsResponse,
  SendVerificationFromPolicyResponse,
} from '@trinsic/service-clients/dist/credentials/models';
import { SubmitVerificationDto } from './dto/submit-verification.dto';
import { SubmitAutoVerificationDto } from './dto/submit-auto-verification.dto';
import { Response } from 'express';
import { SendVerificationDto } from './dto/send-verification.dto';

@Injectable()
export class VerificationService {
  async create(createVerificationDto: CreateVerificationDto) {
    const { policyId } = createVerificationDto;
    let verificationResponse: CreateVerificationFromPolicyResponse;

    try {
      verificationResponse =
        await credentialsClient.createVerificationFromPolicy(policyId);

      logger.info([
        'Create Verification From Policy Response',
        verificationResponse,
      ]);
    } catch (error) {
      logger.error(error);
    }

    return verificationResponse;
  }

  async sendVerificationFromPolicy(sendVerificationDto: SendVerificationDto) {
    const { connectionId, verificationId } = sendVerificationDto;
    const policyId = verificationId;
    let sendVerificationFromPolicyResponse: SendVerificationFromPolicyResponse;

    try {
      sendVerificationFromPolicyResponse =
        await credentialsClient.sendVerificationFromPolicy(
          connectionId,
          policyId,
        );

      return sendVerificationFromPolicyResponse;
    } catch (error) {
      logger.error(error);
    }
  }

  async submitVerification(
    submitVerificationDto: SubmitVerificationDto,
    res: Response,
  ) {
    let submitVerificationResponse;
    const { policyName, credentialId, hidden, walletId, verificationId } =
      submitVerificationDto;

    const parameters = [
      {
        policyName,
        credentialId,
        hidden,
      },
    ];

    try {
      submitVerificationResponse = await walletClient.submitVerification(
        walletId,
        verificationId,
        parameters,
      );

      logger.info(['Submit Verification Response', submitVerificationResponse]);

      return submitVerificationResponse;
    } catch (error) {
      logger.error(error);
    }
  }

  async submitAutoVerification(
    submitAutoVerificationDto: SubmitAutoVerificationDto,
  ) {
    const { walletId, verificationId } = submitAutoVerificationDto;
    let submitAutoVerificationResponse;

    try {
      submitAutoVerificationResponse =
        await walletClient.submitVerificationAutoSelect(
          walletId,
          verificationId,
        );

      logger.info([
        'Submit Verification Auto Select',
        submitAutoVerificationResponse,
      ]);

      console.dir(submitAutoVerificationResponse);
    } catch (error) {
      logger.error(error);
      console.dir(error);
    }
  }

  findOne(id: number) {
    return `This action returns a #${id} verification`;
  }

  update(id: number, updateVerificationDto: UpdateVerificationDto) {
    return `This action updates a #${id} verification`;
  }

  remove(id: number) {
    return `This action removes a #${id} verification`;
  }
}
