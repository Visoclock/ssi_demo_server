import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { VerificationService } from './verification.service';
import { CreateVerificationDto } from './dto/create-verification.dto';
import { UpdateVerificationDto } from './dto/update-verification.dto';
import { SubmitVerificationDto } from './dto/submit-verification.dto';
import { SubmitAutoVerificationDto } from './dto/submit-auto-verification.dto';
import { Response } from 'express';
import { SendVerificationDto } from './dto/send-verification.dto';

@Controller('verification')
export class VerificationController {
  constructor(private readonly verificationService: VerificationService) {}

  @Post('create')
  create(@Body() createVerificationDto: CreateVerificationDto) {
    return this.verificationService.create(createVerificationDto);
  }

  @Post('send-verification')
  sendVerificationFromPolicy(@Body() sendVerificationDto: SendVerificationDto) {
    return this.verificationService.sendVerificationFromPolicy(
      sendVerificationDto,
    );
  }

  @Post('submit-policy-verification')
  submitVerificationFromPolicy(
    @Body() submitVerificationDto: SubmitVerificationDto,
    res: Response,
  ) {
    return this.verificationService.submitVerification(
      submitVerificationDto,
      res,
    );
  }

  @Post('submit-auto-verification')
  submitVerificationFromCredentials(
    @Body() submitAutoVerificationDto: SubmitAutoVerificationDto,
  ) {
    return this.verificationService.submitAutoVerification(
      submitAutoVerificationDto,
    );
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.verificationService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateVerificationDto: UpdateVerificationDto,
  ) {
    return this.verificationService.update(+id, updateVerificationDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.verificationService.remove(+id);
  }
}
