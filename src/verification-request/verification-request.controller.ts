import { Controller, Post, Body, Sse, Query } from '@nestjs/common';
import { VerificationRequestService } from './verification-request.service';
import { CreateVerificationRequestDto } from './dto/create-verification-request.dto';
import { Response } from 'express';

@Controller('verification-request')
export class VerificationRequestController {
  constructor(
    private readonly verificationRequestService: VerificationRequestService,
  ) {}

  @Post()
  create(
    @Body() createVerificationRequestDto: CreateVerificationRequestDto,
    res: Response,
  ) {
    return this.verificationRequestService.create(
      createVerificationRequestDto,
      res,
    );
  }

  @Sse('status')
  getVerificationRequestStatus(
    @Query('uid') uid: string,
    @Query('timestamp') timestamp: number,
  ) {
    return this.verificationRequestService.sendVerificationRequestStatus(
      uid,
      timestamp,
    );
  }
}
