import { Module, HttpModule } from '@nestjs/common';
import { VerificationRequestService } from './verification-request.service';
import { VerificationRequestController } from './verification-request.controller';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [HttpModule, ConfigModule],
  controllers: [VerificationRequestController],
  providers: [VerificationRequestService],
})
export class VerificationRequestModule {}
