import { IsDefined } from 'class-validator';

export class CreateVerificationRequestDto {
  @IsDefined()
  senderUid: string;

  @IsDefined()
  recepientEmail: string;
}
