import { Test, TestingModule } from '@nestjs/testing';
import { VerificationRequestController } from './verification-request.controller';
import { VerificationRequestService } from './verification-request.service';

describe('VerificationRequestController', () => {
  let controller: VerificationRequestController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VerificationRequestController],
      providers: [VerificationRequestService],
    }).compile();

    controller = module.get<VerificationRequestController>(
      VerificationRequestController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
