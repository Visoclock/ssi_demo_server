import { Response } from 'express';
import { User } from './../commons/interfaces/user.interface';
import {
  Injectable,
  HttpService,
  HttpStatus,
  InternalServerErrorException,
} from '@nestjs/common';
import { CreateVerificationRequestDto } from './dto/create-verification-request.dto';
import { ConfigService } from '@nestjs/config';
import { firebaseAdmin, adminTypes, logger } from '../commons/firebaseConfig';
import { IMessageEvent } from '../commons/interfaces/sse-message.interface';
import { from, interval, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { addMinutes } from 'date-fns';

@Injectable()
export class VerificationRequestService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
  ) {}

  async create(
    createVerificationRequestDto: CreateVerificationRequestDto,
    res: Response,
  ) {
    const { senderUid, recepientEmail } = createVerificationRequestDto;

    let query, doc;

    try {
      query = await firebaseAdmin
        .firestore()
        .collection('users')
        .where('primary_email', '==', `${recepientEmail}`)
        .get();

      doc = query.docs[0];

      logger.info(doc);

      if (doc === undefined || !doc.exists) {
        return res.status(HttpStatus.NOT_FOUND).json('User not found!');
      }
    } catch (error) {
      throw new InternalServerErrorException(error);
    }

    const userData = doc.data() as User;
    const currTimeStamp = Date.now();

    logger.info(userData);

    try {
      await firebaseAdmin
        .firestore()
        .collection('verificationRequest')
        .doc(userData.uuid)
        .collection('requests')
        .doc(`${currTimeStamp}`)
        .set({
          recipient_id: userData.uuid,
          sender_id: senderUid,
          status: 'NEW',
          idRequest: false,
          expires: adminTypes.firestore.Timestamp.fromDate(
            addMinutes(new Date(), 30),
          ),
          date_created: adminTypes.firestore.Timestamp.fromDate(new Date()),
        });

      const buffer = Buffer.from(`${currTimeStamp}`, 'utf-8');
      const base64Str = buffer.toString('base64');

      logger.info(base64Str);

      return {
        responseCode: `${base64Str}`,
        uid: userData.uuid,
        timestamp: currTimeStamp,
      };
    } catch (error) {
      throw new InternalServerErrorException(error, 'An Error Occured!');
    }
  }

  sendVerificationRequestStatus(uid: string, timestamp: number) {
    const message: IMessageEvent = {
      data: {
        status: 'NEW',
      },
    };

    const docRef = firebaseAdmin
      .firestore()
      .collection('verificationRequest')
      .doc(uid)
      .collection('requests')
      .doc(`${timestamp}`);

    docRef.onSnapshot((snapshot) => {
      const data = snapshot.data();

      if (data?.status === 'VERIFIED') {
        message.data = { status: 'VERIFIED' };
      }
    });

    return interval(1000).pipe(map((_) => message));
  }
}
