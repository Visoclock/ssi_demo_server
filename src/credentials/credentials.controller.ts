import { Controller, Get, Post, Body, Param, Query } from '@nestjs/common';
import { CredentialsService } from './credentials.service';
import { CreateCredentialDto } from './dto/create-credential.dto';
import { IssueCredentialDto } from './dto/issue-credential.dto';
import { ListCredentialsDto } from './dto/list-credentials.dto';
import { Response } from 'express';

@Controller('credentials')
export class CredentialsController {
  constructor(private readonly credentialsService: CredentialsService) {}

  @Post('create')
  createCredential(@Body() createCredentialDto: CreateCredentialDto) {
    return this.credentialsService.createCredential(createCredentialDto);
  }

  @Post('issue')
  issueCredential(@Body() issueCredentialDto: IssueCredentialDto) {
    return this.credentialsService.issueCredential(issueCredentialDto);
  }

  @Get()
  listCredentials(
    @Query('connectionId') connectionId: string,
    @Query('state') state: string,
    @Query('definitionId') definitionId: string,
  ) {
    return this.credentialsService.listCredentials(
      connectionId,
      state,
      definitionId,
    );
  }

  @Get('connections')
  listConnections(@Query('state') state: string) {
    if (state) {
      return this.credentialsService.listConnections(state);
    } else {
      return this.credentialsService.listConnections();
    }
  }
}
