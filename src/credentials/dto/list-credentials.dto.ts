import { IsDefined, IsOptional } from 'class-validator';

export class ListCredentialsDto {
  @IsOptional()
  connectionId: string | null;

  @IsOptional()
  state?: string | null;

  @IsOptional()
  definitionId?: string | null;
}
