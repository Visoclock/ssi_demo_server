import { IsDefined, IsOptional } from 'class-validator';

export class CreateCredentialDto {
  @IsDefined()
  definition_id: string;

  @IsDefined()
  voice_id: string;

  @IsDefined()
  email: string;

  @IsDefined()
  user_id: string;

  @IsOptional()
  face_id?: string;
}
