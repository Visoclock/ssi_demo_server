import { IsDefined, ValidateNested } from 'class-validator';

export class IssueCredentialDto {
  @IsDefined()
  credentialId: string;

  @ValidateNested()
  attributes: CredentialAttributes;
}

interface CredentialAttributes {
  voice_id: string;
  email: string;
  user_id: string;
}
