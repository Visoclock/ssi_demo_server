import {
  HttpException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { CreateCredentialDto } from './dto/create-credential.dto';
import { credentialsClient, walletClient } from '../commons/trinsicConfig';
import { logger } from '../commons/firebaseConfig';
import {
  AcceptInvitationResponse,
  CreateWalletResponse,
} from '@trinsic/service-clients/dist/wallet/models';
import { IssueCredentialDto } from './dto/issue-credential.dto';
import { ListCredentialsDto } from './dto/list-credentials.dto';
import { Response } from 'express';
import {
  CreateConnectionResponse,
  CredentialsServiceClientListConnectionsOptionalParams,
  CredentialsServiceClientListCredentialsOptionalParams,
  ListConnectionsResponse,
} from '@trinsic/service-clients/dist/credentials/models';

export interface TrinsicErrorResponse {
  statusCode: number;
  request: Record<string, unknown>;
  response: {
    body: string;
    headers: Record<string, unknown>;
    status: number;
  };
}

@Injectable()
export class CredentialsService {
  async createCredential(createCredentialDto: CreateCredentialDto) {
    const { definition_id, voice_id, email, user_id, face_id } =
      createCredentialDto;

    // Create Connection
    const name = null;
    const connectionId = null;
    const multiParty = false;

    const connection = await this._createConnection(
      name,
      connectionId,
      multiParty,
    );

    // Create Wallet
    const walletResponse = await this._createWallet(email, user_id);

    // Accept Connection Invitation
    const acceptConnectionResponse = await this._acceptConnectionInvitation(
      walletResponse.walletId,
      connection.invitation,
    );

    // Create and Offer User Credential
    const userCredential = await credentialsClient.createCredential({
      definitionId: definition_id,
      connectionId: connection.connectionId,
      automaticIssuance: true,
      credentialValues: {
        voice_id,
        email,
        face_id,
      },
    });

    logger.info(['User Credential Response', userCredential]);

    // Accept Credential Offer
    await this._acceptCredentialOffer(
      walletResponse.walletId,
      userCredential.credentialId,
    );

    return userCredential;
  }

  async issueCredential(issueCredentialDto: IssueCredentialDto) {
    // Optionally issue a credential with different values than offered
    const newCredentialValues = { ...issueCredentialDto.attributes };
    await credentialsClient.issueCredential(issueCredentialDto.credentialId, {
      body: newCredentialValues,
    });
  }

  async listWallets() {
    const wallets = await walletClient.listWallets();

    return wallets;
  }

  async listCredentials(
    connectionId: string,
    state: string,
    definitionId: string,
  ) {
    // ConnectionId Can be null | <connection identifier>
    // State Can be null | "Offered" | "Requested" | "Issued" | "Rejected" | "Revoked"
    // DefinitionId Can be null | <definition identifier>
    const requestParam: CredentialsServiceClientListCredentialsOptionalParams =
      {
        connectionId: connectionId || null,
        definitionId: definitionId || null,
      };

    if (state) {
      if (state.toLowerCase() === 'offered') {
        requestParam.state = 'Offered';
      } else if (state.toLowerCase() === 'requested') {
        requestParam.state = 'Requested';
      } else if (state.toLowerCase() === 'issued') {
        requestParam.state = 'Issued';
      } else if (state.toLowerCase() === 'rejected') {
        requestParam.state = 'Rejected';
      } else if (state.toLowerCase() === 'revoked') {
        requestParam.state = 'Revoked';
      }
    } else {
      requestParam.state = null;
    }

    try {
      const credentials = await credentialsClient.listCredentials(requestParam);
      logger.info(['List Credentials: Verifier Side', credentials]);
      console.dir(credentials);

      return credentials;
    } catch (error) {
      logger.error(error);
      console.error(error);
      throw new InternalServerErrorException(error, 'Internal Server Error');
    }
  }

  async listConnections(state?: string): Promise<ListConnectionsResponse> {
    // State Can be null | "Invited" | "Negotiating" | "Connected"
    let connectionState: CredentialsServiceClientListConnectionsOptionalParams;

    if (state.toLowerCase() === 'invited') {
      connectionState = { state: 'Invited' };
    } else if (state.toLowerCase() === 'negotiating') {
      connectionState = { state: 'Negotiating' };
    } else if (state.toLowerCase() === 'connected') {
      connectionState = { state: 'Connected' };
    } else {
      connectionState = null;
    }

    try {
      const connections = await credentialsClient.listConnections(
        connectionState,
      );
      logger.log(['List All Connections: Verifier Side', connections]);
      console.dir(connections);

      return connections;
    } catch (error) {
      logger.error(error);
      console.error(error);
      throw new InternalServerErrorException(error, 'Internal Server Error');
    }
  }

  async _createConnection(
    name: string,
    connectionId: string,
    multiParty: boolean,
  ) {
    let connectionResponse: CreateConnectionResponse;

    try {
      connectionResponse = await credentialsClient.createConnection({
        name,
        connectionId,
        multiParty,
      });

      logger.info(['Connection Response', connectionResponse]);
    } catch (error) {
      logger.error(error);
    }

    return connectionResponse;
  }

  async _createWallet(email: string, walletId: string) {
    let walletResponse: CreateWalletResponse;

    try {
      walletResponse = await walletClient.createWallet({
        ownerName: email,
        walletId: walletId,
      });

      logger.info(['Wallet Response', walletResponse]);
    } catch (error) {
      logger.error(error);
    }

    return walletResponse;
  }

  async _acceptConnectionInvitation(walletId: string, invitation: string) {
    let acceptInvitationResponse: AcceptInvitationResponse;
    try {
      acceptInvitationResponse = await walletClient.acceptInvitation(
        walletId,
        invitation,
      );

      logger.info(['Accept Connection Response', acceptInvitationResponse]);
    } catch (error) {
      logger.error(error);
    }

    return acceptInvitationResponse;
  }

  async _acceptCredentialOffer(walletId: string, credentialId: string) {
    try {
      const walletAcceptCredentialResponse =
        await walletClient.acceptCredentialOffer(walletId, credentialId);

      logger.info([
        'Wallet Accept Credential Response',
        walletAcceptCredentialResponse,
      ]);
    } catch (error) {
      logger.error(error);
    }
  }
}

// const wallets = this.listWallets();

// logger.info(await wallets);

// try {
//   await credentialsClient.issueCredential(userCredential.credentialId, {
//     body: null,
//   });
// } catch (error) {
//   logger.error(error);
//   throw new HttpException(error.)
// }
