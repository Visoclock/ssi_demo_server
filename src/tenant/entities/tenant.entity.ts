export class Tenant {
  tenantId: string;
  issuerSeed?: string;
  name: string;
  imageUrl?: string;
  networkId?: string;
  endorserType: 'Shared' | 'Dedicated' | 'Delegated';
  region?: string;
}
