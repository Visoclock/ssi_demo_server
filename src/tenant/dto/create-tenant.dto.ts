import { IsDefined, IsOptional } from 'class-validator';

export class CreateTenantDto {
  @IsDefined()
  name: string;

  @IsOptional()
  imageUrl?: string;

  @IsOptional()
  networkId?: string;

  @IsDefined()
  endorserType: 'Shared' | 'Dedicated' | 'Delegated';

  @IsOptional()
  region?: string;
}
