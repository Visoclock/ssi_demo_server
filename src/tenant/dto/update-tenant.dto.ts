import { PartialType } from '@nestjs/mapped-types';
import { IsDefined, IsOptional } from 'class-validator';
import { CreateTenantDto } from './create-tenant.dto';

export class UpdateTenantDto extends PartialType(CreateTenantDto) {
  @IsDefined()
  name: string;

  @IsOptional()
  imageUrl: string;
}
