import { Injectable } from '@nestjs/common';
import { CreateTenantDto } from './dto/create-tenant.dto';
import { UpdateTenantDto } from './dto/update-tenant.dto';
import { providerClient } from '../commons/trinsicConfig';
import { CreateTenantResponse } from '@trinsic/service-clients/dist/credentials/models';
import { firebaseAdmin } from '../commons/firebaseConfig';
import { Tenant } from './entities/tenant.entity';

@Injectable()
export class TenantService {
  async create(
    createTenantDto: CreateTenantDto,
  ): Promise<CreateTenantResponse> {
    const tenant: Tenant = {
      tenantId: '',
      name: createTenantDto.name,
      endorserType: 'Shared',
      imageUrl: createTenantDto.imageUrl,
    };

    firebaseAdmin.firestore().settings({
      ignoreUndefinedProperties: true,
    });

    const doc = await firebaseAdmin.firestore().collection('tenants').doc();
    const docId = doc.id;

    tenant.tenantId = docId;

    await firebaseAdmin
      .firestore()
      .collection('tenants')
      .doc(docId)
      .set(tenant);

    return providerClient.createTenant(tenant);
  }

  findAll() {
    return providerClient.listTenants();
  }

  findOne(id: string) {
    return providerClient.getTenant(id);
  }

  update(id: string, updateTenantDto: UpdateTenantDto) {
    return providerClient.updateTenant(id, updateTenantDto);
  }

  remove(id: string) {
    return providerClient.deleteTenant(id);
  }
}
