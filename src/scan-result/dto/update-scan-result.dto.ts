import { PartialType } from '@nestjs/mapped-types';
import { CreateScanResultDto } from './create-scan-result.dto';

export class UpdateScanResultDto extends PartialType(CreateScanResultDto) {}
