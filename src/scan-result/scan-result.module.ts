import { Module } from '@nestjs/common';
import { ScanResultService } from './scan-result.service';
import { ScanResultController } from './scan-result.controller';

@Module({
  controllers: [ScanResultController],
  providers: [ScanResultService],
})
export class ScanResultModule {}
