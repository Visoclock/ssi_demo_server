import { Injectable } from '@nestjs/common';
import { CreateScanResultDto } from './dto/create-scan-result.dto';
import { UpdateScanResultDto } from './dto/update-scan-result.dto';

@Injectable()
export class ScanResultService {
  create(createScanResultDto: CreateScanResultDto) {
    return 'This action adds a new scanResult';
  }

  findAll() {
    return `This action returns all scanResult`;
  }

  findOne(id: number) {
    return `This action returns a #${id} scanResult`;
  }

  update(id: number, updateScanResultDto: UpdateScanResultDto) {
    return `This action updates a #${id} scanResult`;
  }

  remove(id: number) {
    return `This action removes a #${id} scanResult`;
  }
}
