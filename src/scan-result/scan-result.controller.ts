import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ScanResultService } from './scan-result.service';
import { CreateScanResultDto } from './dto/create-scan-result.dto';
import { UpdateScanResultDto } from './dto/update-scan-result.dto';

@Controller('scan-result')
export class ScanResultController {
  constructor(private readonly scanResultService: ScanResultService) {}

  @Post()
  create(@Body() createScanResultDto: CreateScanResultDto) {
    return this.scanResultService.create(createScanResultDto);
  }

  @Get()
  findAll() {
    return this.scanResultService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.scanResultService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateScanResultDto: UpdateScanResultDto,
  ) {
    return this.scanResultService.update(+id, updateScanResultDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.scanResultService.remove(+id);
  }
}
