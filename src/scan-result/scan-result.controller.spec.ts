import { Test, TestingModule } from '@nestjs/testing';
import { ScanResultController } from './scan-result.controller';
import { ScanResultService } from './scan-result.service';

describe('ScanResultController', () => {
  let controller: ScanResultController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ScanResultController],
      providers: [ScanResultService],
    }).compile();

    controller = module.get<ScanResultController>(ScanResultController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
