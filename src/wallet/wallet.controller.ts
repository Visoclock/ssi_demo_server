import { Controller, Get, Post, Body, Param, Delete } from '@nestjs/common';
import { WalletService } from './wallet.service';
import { CreateWalletDto } from './dto/create-wallet.dto';
import { AcceptInvitationDto } from './dto/accept-invitation.dto';
import { SubmitAutoVerificationDto } from '../verification/dto/submit-auto-verification.dto';

@Controller('wallets')
export class WalletController {
  constructor(private readonly walletService: WalletService) {}

  @Post()
  create(@Body() createWalletDto: CreateWalletDto) {
    return this.walletService.create(createWalletDto);
  }

  @Get(':walletId/connections')
  listAllConnections(@Param('walletId') walletId: string) {
    return this.walletService.listAllConnections(walletId);
  }

  @Get(':walletId/credentials')
  listAllCredentials(@Param('walletId') walletId: string) {
    return this.walletService.listAllCredentials(walletId);
  }

  @Get(':walletId/connections/:connectionId')
  getConnectionById(
    @Param('walletId') walletId: string,
    @Param('connectionId') connectionId: string,
  ) {
    return this.walletService.getConnectionById(walletId, connectionId);
  }

  @Get(':walletId/connections/invitations')
  listConnectionInvitations(@Param('walletId') walletId: string) {
    return this.walletService.listConnectionInvitations(walletId);
  }

  @Post(':walletId/connections/invitation')
  acceptConnectionInvitation(@Body() payload: AcceptInvitationDto) {
    return this.walletService.acceptInvitation(payload);
  }

  @Get(':walletId/verifications/:verificationId')
  listCredentials(
    @Param('walletId') walletId: string,
    @Param('verificationId') verificationId: string,
  ) {
    return this.walletService.getAvailableCredentialsForVerification(
      walletId,
      verificationId,
    );
  }

  @Post('verification')
  submitVerificationAutoSelect(
    @Body() submitVerificationDto: SubmitAutoVerificationDto,
  ) {
    return this.walletService.submitVerificationAutoSelect(
      submitVerificationDto,
    );
  }

  @Get(':walletId/verifications')
  listAllVerifications(@Param('walletId') walletId: string) {
    return this.walletService.listAllVerifications(walletId);
  }

  @Delete(':walletId/connections/:connectionId')
  deleteConnection(
    @Param('walletId') walletId: string,
    @Param('connectionId') connectionId: string,
  ) {
    return this.walletService.deleteConnection(walletId, connectionId);
  }
}
