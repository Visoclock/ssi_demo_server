export interface AcceptInvitationDto {
  walletId: string;
  invitation: any;
}
