import { HttpException, Injectable, HttpStatus } from '@nestjs/common';
import { CreateWalletDto } from './dto/create-wallet.dto';
import { UpdateWalletDto } from './dto/update-wallet.dto';
import { walletClient } from '../commons/trinsicConfig';
import { AcceptInvitationDto } from './dto/accept-invitation.dto';
import {
  CreateWalletResponse,
  ListCredentialsResponse,
} from '@trinsic/service-clients/dist/wallet/models';
import { firebaseAdmin, logger } from '../commons/firebaseConfig';
import firebase from 'firebase';
import { SubmitVerificationDto } from '../verification/dto/submit-verification.dto';
import { SubmitAutoVerificationDto } from '../verification/dto/submit-auto-verification.dto';
import { ListVerificationsResponse } from '@trinsic/service-clients/dist/credentials/models';

@Injectable()
export class WalletService {
  async create(createWalletDto: CreateWalletDto) {
    const ownerName = createWalletDto.ownerName;
    const walletId = createWalletDto.walletId;
    const wallet = (await walletClient.createWallet({
      ownerName: ownerName,
      walletId: walletId,
    })) as CreateWalletResponse;

    const walletDoc = {
      walletId: wallet.walletId,
      owner: ownerName,
      name: wallet.name,
      dateCreated: firebase.firestore.Timestamp.now(),
    };

    await firebaseAdmin.firestore().collection('wallets').add(walletDoc);
    return walletDoc;
  }

  listAllConnections(walletId: string) {
    return walletClient.listConnections(walletId);
  }

  async listAllCredentials(walletId: string) {
    let listCredentialsResponse: ListCredentialsResponse;

    try {
      listCredentialsResponse = await walletClient.listCredentials(walletId);

      logger.info([
        'List Wallet Credentials Response',
        listCredentialsResponse,
      ]);

      console.dir(listCredentialsResponse);
    } catch (error) {
      logger.error(error);
    }

    return listCredentialsResponse;
  }

  async listAllVerifications(walletId: string) {
    let verifications: ListVerificationsResponse;

    try {
      verifications = await walletClient.listVerifications(walletId);
      logger.info(verifications);
      console.dir(verifications);

      return verifications;
    } catch (error) {
      logger.error(error);
      console.dir(error);

      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async submitVerificationAutoSelect(params: SubmitAutoVerificationDto) {
    const { walletId, verificationId } = params;

    try {
      await walletClient.submitVerificationAutoSelect(walletId, verificationId);
    } catch (error) {
      logger.error(error);
      console.error(error);
    }
  }

  getConnectionById(walletId: string, connectionId: string) {
    return walletClient.getConnection(walletId, connectionId);
  }

  async getAvailableCredentialsForVerification(
    walletId: string,
    verificationId: string,
  ) {
    let credentials;
    try {
      credentials = await walletClient.getAvailableCredentialsForVerification(
        walletId,
        verificationId,
      );

      logger.info([
        'List Available Credentials for Verification Response',
        credentials,
      ]);
    } catch (error) {
      logger.error(error);
    }

    return credentials;
  }

  listConnectionInvitations(walletId: string) {
    return walletClient.listInvitations(walletId);
  }

  acceptInvitation(payload: AcceptInvitationDto) {
    return walletClient.acceptInvitation(payload.walletId, payload.invitation);
  }

  deleteConnection(walletId: string, connectionId) {
    return walletClient.deleteConnection(walletId, connectionId);
  }
}
