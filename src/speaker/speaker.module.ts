import { HttpModule, Module } from '@nestjs/common';
import { SpeakerService } from './speaker.service';
import { SpeakerController } from './speaker.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    HttpModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        baseURL: configService.get('azureBaseURL'),
        headers: {
          'Content-Type': 'application/json',
          'Ocp-Apim-Subscription-Key': configService.get(
            'azureSubscriptionKey',
          ),
        },
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [SpeakerController],
  providers: [SpeakerService],
})
export class SpeakerModule {}
