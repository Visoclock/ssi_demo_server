import { HttpService, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { CreateSpeakerDto } from './dto/create-speaker.dto';
import { UpdateSpeakerDto } from './dto/update-speaker.dto';
import { Response } from 'express';

@Injectable()
export class SpeakerService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
  ) {}

  createProfile(createSpeakerDto: CreateSpeakerDto, res: Response) {
    return this.httpService
      .post('/text-dependent/profiles', createSpeakerDto)
      .toPromise()
      .then(
        (result) => {
          const { data } = result;
          return res.status(HttpStatus.CREATED).json(data);
        },
        (error) => {
          const { data } = error.response;
          switch (error.response.status) {
            case 400:
              return res.status(400).json(data);
            case 401:
              return res.status(401).json(data);
            case 403:
              return res.status(401).json(data);
            case 500:
              return res.status(500).json(data);
            default:
              return res.status(500).json({ error: ' something went wrong' });
          }
        },
      )
      .catch(() => {
        return res.status(500).json({ error: ' something went wrong' });
      });
  }

  createEnrollment() {
    return `HEllo World!`;
  }

  listPhrases(locale: string, res: Response) {
    return this.httpService
      .get(`/text-dependent/phrases/${locale}`)
      .toPromise()
      .then(
        (result) => {
          const { data } = result;
          return res.status(HttpStatus.OK).json(data);
        },
        (error) => {
          const { data } = error.response;
          switch (error.response.status) {
            case 400:
              return res.status(400).json(data);
            case 401:
              return res.status(401).json(data);
            case 403:
              return res.status(401).json(data);
            case 500:
              return res.status(500).json(data);
            default:
              return res.status(500).json({ error: ' something went wrong' });
          }
        },
      )
      .catch(() => {
        return res.status(500).json({ error: ' something went wrong' });
      });
  }

  verifyProfile(profileId: string) {
    return this.httpService.post(
      `/text-dependent/profiles/${profileId}/verify`,
    );
  }

  getProfile(profileId: string, res: Response) {
    return this.httpService
      .get(`/text-dependent/profiles/${profileId}`)
      .toPromise()
      .then(
        (result) => {
          const { data } = result;
          return res.status(200).json(data);
        },
        (error) => {
          const { data } = error.response;
          switch (error.response.status) {
            case 400:
              return res.status(400).json(data);
            case 401:
              return res.status(401).json(data);
            case 403:
              return res.status(401).json(data);
            case 500:
              return res.status(500).json(data);
            default:
              return res.status(500).json({ error: ' something went wrong' });
          }
        },
      )
      .catch(() => {
        return res.status(500).json({ error: ' something went wrong' });
      });
  }

  update(id: number, updateSpeakerDto: UpdateSpeakerDto) {
    return `This action updates a #${id} speaker`;
  }

  remove(id: number) {
    return `This action removes a #${id} speaker`;
  }
}
