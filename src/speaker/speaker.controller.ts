import { Response } from 'express';
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
} from '@nestjs/common';
import { SpeakerService } from './speaker.service';
import { CreateSpeakerDto } from './dto/create-speaker.dto';
import { UpdateSpeakerDto } from './dto/update-speaker.dto';
import { CreateSpeakerProfileDto } from './dto/create-speaker-profile.dto';

@Controller('speaker')
export class SpeakerController {
  constructor(private readonly speakerService: SpeakerService) {}

  @Post('text-dependent/create-profile')
  create(@Body() createSpeakerDto: CreateSpeakerDto, @Res() res: Response) {
    return this.speakerService.createProfile(createSpeakerDto, res);
  }

  @Post('text-dependent/profiles/:profileId/enrollments')
  createEnrollment(@Body() createEnrollmentDto) {
    return this.speakerService.createEnrollment();
  }

  @Get('text-dependent/list-phrases/:locale')
  listPhrases(@Param('locale') locale: string, @Res() res: Response) {
    return this.speakerService.listPhrases(locale, res);
  }

  @Post('text-dependent/verify-profiles/:profileId')
  verifyProfile(@Param('profileId') profileId: string) {
    return this.speakerService.verifyProfile(profileId);
  }

  @Get(':profileId')
  update(@Param('profileId') profileId: string, res: Response) {
    return this.speakerService.getProfile(profileId, res);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.speakerService.remove(+id);
  }
}
