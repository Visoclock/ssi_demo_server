import { IsDefined } from 'class-validator';

export class CreateSpeakerProfileDto {
  @IsDefined()
  locale: string;
}
